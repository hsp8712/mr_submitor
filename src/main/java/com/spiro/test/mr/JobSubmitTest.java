package com.spiro.test.mr;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 *
 * java -Dhadoop.home.dir=/opt/hadoop-2.7.4 -Djava.library.path=/opt/hadoop-2.7.4/lib/native
 * -classpath mr_submitor-1.0-SNAPSHOT.jar:asm-5.2.jar:slf4j-api-1.7.25.jar cn.com.tiza.mr.JobSubmitTest
 * "$(/opt/hadoop/bin/hadoop classpath):/root/mr_submitor/ship-mr/dist/ship-mr.jar:/root/mr_submitor/ship-mr/conf"
 * "/root/mr_submitor/ship-mr/lib/*"
 *
 *
 * @Author: Shaoping Huang
 * @Description:
 * @Date: 1/4/2018
 */
public class JobSubmitTest {

    public static void main(String[] args) {

        String classPath = args[0];
        String libJars = args[1];

        try {
            MapReduceClassLoader cl = new MapReduceClassLoader();
            cl.addClassPath(classPath);
            cl.addClassPath(libJars);

            System.out.println("URLS:" + Arrays.toString(cl.getURLs()));

            Thread.currentThread().setContextClassLoader(cl);

            Class mainClass = cl.loadClass("cn.com.tiza._724.ship.etl._2ci.Main");
            System.out.println(mainClass.getClassLoader());
            Method mainMethod = mainClass.getMethod("main", new Class[] { String[].class });

            String libJarsParam = ClassPathUtils.getClassPathWithDotSep(libJars);

            System.out.println("libJarsParam: " + libJarsParam);

            String[] firstParams = new String[] { "-libjars", libJarsParam };
            Object[] params = new Object[] {firstParams};
            mainMethod.invoke(null, params);

            Class jobClass = cl.loadClass("org.apache.hadoop.mapreduce.Job");
            System.out.println(jobClass.getClassLoader());
            Field field = jobClass.getField(JobAdapter.JOB_FIELD_NAME);
            System.out.println(field.get(null));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
