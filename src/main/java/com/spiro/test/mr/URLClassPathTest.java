package com.spiro.test.mr;

import java.net.URL;
import java.util.Arrays;

/**
 * @Author: Shaoping Huang
 * @Description:
 * @Date: 1/5/2018
 */
public class URLClassPathTest {
    public static void main(String[] args) {

        String classPath = args[0];
        URL[] urls = ClassPathUtils.getClassPathURLs(classPath);
        System.out.println("urls = " + Arrays.toString(urls));

    }
}
